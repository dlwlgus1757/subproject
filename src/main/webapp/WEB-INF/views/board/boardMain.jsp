<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<style>SS
	body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
	.w3-third img{margin-bottom: -6px; opacity: 0.8; cursor: pointer}
	.w3-third img:hover{opacity: 1}
	tr {
		cursor:pointer;
	}
	.modal-mask {
	  position: fixed;
	  z-index: 9998;
	  top: 0;
	  left: 0;
	  width: 100%;
	  height: 100%;
	  background-color: rgba(0, 0, 0, .5);
	  display: table;
	  transition: opacity .3s ease;
	}
	
	.modal-wrapper {
	  display: table-cell;
	  vertical-align: middle;
	}
	
	.modal-container {
	  width: 300px;
	  margin: 0px auto;
	  padding: 20px 30px;
	  background-color: #fff;
	  border-radius: 2px;
	  box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
	  transition: all .3s ease;
	  font-family: Helvetica, Arial, sans-serif;
	} 
	
	.modal-header h3 {
	  margin-top: 0;
	  color: #42b983;
	}
	
	.modal-body {
	  margin: 20px 0; 
	}
	
	.modal-default-button {
	  float: right;
	}
	
	/*
	 * The following styles are auto-applied to elements with
	 * transition="modal" when their visibility is toggled
	 * by Vue.js.
	 *
	 * You can easily play with the modal transition by editing
	 * these styles.
	 */
	
	.modal-enter {
	  opacity: 0;
	}
	
	.modal-leave-active {
	  opacity: 0;
	}
	
	.modal-enter .modal-container,
	.modal-leave-active .modal-container {
	  -webkit-transform: scale(1.1);
	  transform: scale(1.1);
	}
		
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<body class="w3-light-grey w3-content" style="max-width:1600px">

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-bar-block w3-white w3-animate-left w3-text-grey w3-collapse w3-top w3-center" style="z-index:3;width:300px;font-weight:bold" id="mySidebar"><br>
  <h3 class="w3-padding-64 w3-center"><b>SOME<br>NAME</b></h3>
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-padding w3-hide-large">CLOSE</a>
  <a href="/board/boardMain" onclick="w3_close()" class="w3-bar-item w3-button">BOARD</a> 
  <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button">MESSAGE</a> 
  <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button">LOGIN</a>
</nav>

<!-- Top menu on small screens -->
<header class="w3-container w3-top w3-hide-large w3-white w3-xlarge w3-padding-16">
  <span class="w3-left w3-padding">SOME NAME</span>
  <a href="javascript:void(0)" class="w3-right w3-button w3-white" onclick="w3_open()">☰</a>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px; ">

  <!-- Push down content on small screens --> 
  <div class="w3-hide-large" style="margin-top:83px"></div>
  
  <div class="w3-row w3-container" style="min-height:870px;">
		<div class="w3-center" style="margin-top:30px;"><h1>Board</h1></div>
		<div class="table-section w3-padding-32" style="min-height:700px">
			<table class="w3-table-all w3-centered" id="boardTable">
				<colgroup>
					<col width="10%">
					<col width="50%">
					<col width="20%">
					<col width="20%">
				</colgroup>
			    <thead>
			      <tr class="w3-black">
			      	<th>No.</th>
			        <th>Title</th>
			        <th>Writer</th>
			        <th>Date</th>
			      </tr>
			    </thead>
			    <tbody id="boardBody">
				    <template>
				    	<table-row v-for="data in datas" v-bind:data="data" v-bind:key="data.bno"></table-row>
					</template>
			    </tbody>
			  </table>
		</div>
		<div class="btn-section w3-row">
			<div class="w3-col m1">
				<button class="w3-btn w3-black" onclick="document.getElementById('id01').style.display='block'">write</button>
				<div id="id01" class="w3-modal">
				    <div class="w3-modal-content w3-animate-top w3-card-4">
				   	 <form action="#" method="post">
					      <header class="w3-container w3-black"> 
					        <span onclick="document.getElementById('id01').style.display='none'" 
					        class="w3-button w3-display-topright">&times;</span>
					        <h2>Write</h2>
					      </header>
					      <div class="w3-container">
					        <div class="w3-row w3-padding">
					        	<div class="w3-col m1 w3-center">
					        		<h5>Title</h5>
					        	</div>
					        	<div class="w3-col m11">
					        		<!-- <input type="text" class="w3-input" id="bTitle" name="bTitle"> -->
					        		<custom-input data></custom-input>
					        	</div>
					        </div>
					        <div class="w3-row w3-padding">
					        	<div class="w3-col m1 w3-center">
					        		<h5>Date</h5>
					        	</div>
					        	<div class="w3-col m5">
					        		<input type="date" class="w3-input" id="bDate" name="bDate">
					        	</div>
					        	<div class="w3-col m1 w3-center">
					        		<h5>Writer</h5>
					        	</div>
					        	<div class="w3-col m5">
					        		<input type="text" class="w3-input" id="bName" name="bName">
					        	</div>
					        </div>
					        <div class="w3-row w3-padding w3-margin-bottom">
					        	<h5>Content</h5>
					        	<textarea id="bContent" name="bContent" class="w3-input w3-border w3-round" rows="15" cols="50"></textarea>
					        </div>
					      </div>
					      <footer class="w3-container w3-black w3-padding">
					      	<button type="button" id="writeButton" class="w3-btn w3-white w3-right">Write</button>
					      </footer>
				      </form>
			    	</div>
		    	</div>
		    	
		    	<div id="id02" class="w3-modal">
		    		<template>
				    <div class="w3-modal-content w3-animate-top w3-card-4">
				   	 <form action="#" method="post">
					      <header class="w3-container w3-black"> 
					        <span onclick="closeModal()" 
					        class="w3-button w3-display-topright">&times;</span>
					        <h2>Write</h2>
					      </header>
					      <div class="w3-container">
					        <div class="w3-row w3-padding">
					        	<div class="w3-col m1 w3-center">
					        		<h5>Title</h5>
					        	</div>
					        	<div class="w3-col m11">
					        		<!-- <h5>{{info.bTitle}}</h5> -->
					        		<input type="text" class="w3-input" id="bTitle" name="bTitle" v-model="info.bTitle">
					        	</div>
					        </div>
					        <div class="w3-row w3-padding">
					        	<div class="w3-col m1 w3-center">
					        		<h5>Date</h5>
					        	</div>
					        	<div class="w3-col m5">
					        		<input type="text" class="w3-input" id="bTitle" name="bTitle" v-model="info.bDate.substring(0,10)" readonly>
					        	</div>
					        	<div class="w3-col m1 w3-center">
					        		<h5>Writer</h5>
					        	</div>
					        	<div class="w3-col m5">
					        		<input type="text" class="w3-input" id="bTitle" name="bTitle" v-model="info.bName" readonly>
					        	</div>
					        </div>
					        <div class="w3-row w3-padding w3-margin-bottom">
					        	<h5>Content</h5>
					        	<textarea id="bContent" name="bContent" class="w3-input w3-border w3-round" rows="15" cols="50">{{info.bContent}}</textarea>
					        </div>
					      </div>
					      <footer class="w3-container w3-black w3-padding">
					      	<button type="button" id="updateButton" v-on:click="updateTarget" class="w3-btn w3-white w3-right">Update</button>
					      	<button type="button" id="deleteButton" v-on:click="deleteTarget" class="w3-btn w3-white w3-right">Delete</button>
					      </footer>
				      </form>
			    	</div>
			    	</template>
		    	</div>
			</div>
			<div class="w3-col m11 w3-row">
				<input class="w3-input w3-col m9" type="text" placeholder="Search">
				<button class="w3-col m3 w3-btn w3-black">Search</button>
			</div>
		</div>
  </div>

  <div class="w3-black w3-center w3-padding-24 w3-bottom">Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-opacity">w3.css</a></div>
<!-- End page content -->
</div>

<script>
// Script to open and close sidebar
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("myOverlay").style.display = "none";
}


$("#writeButton").on("click", function(e){
	e.preventDefault();
	var board = {
		bTitle:$("#bTitle").val(),
		bName:$("#bName").val(),
		bContent:$("#bContent").val(),
		bTitle:$("#bTitle").val(),
		bDate:$("#bDate").val()	
	}

	console.log(board);

	$.ajax({
		type:"post",
		url:"/board/add",
		dataType:'json',
		contentType:'application/json; charset=utf-8',
		data:JSON.stringify(board)
	}).done(function(){
		alert("작성이 완료되었습니다.");
		location.reload();
	}).fail(function(error) {
		alert("작성이 완료되었습니다!");
		location.reload();
	});
	
});
function closeModal(){
	document.getElementById('id02').style.display='none';
	location.reload();
}
</script>

<script type="module">
import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.0/dist/vue.esm.browser.js'
var modal = document.getElementById("id02");


Vue.component('table-row', {
	props:['data', 'modal'],
	template:'<tr v-on:click="showLog"><td>{{data.bno}}</td><td>{{data.bTitle}}</td><td>{{data.bName}}</td><td>{{data.bDate.substring(0, 10)}}</td></tr>',
	methods: {
         showLog: function() {
			var key = this.data;
			console.log(key);
			new Vue({
  				el: '#id02',
  				data: {
    				info:key
  				},
				methods:{
					deleteTarget:function(event) {
						axios.post('http://127.0.0.1:8888/board/boardDelete', 
									{bno:this.info.bno})
									.then(function(){
										alert("삭제가 완료되었습니다.");
										location.reload();
									});
					},
					updateTarget:function(event) {
						axios.post('http://127.0.0.1:8888/board/boardUpdate',
									{bno:this.info.bno, bTitle:this.info.bTitle, bContent:this.info.bContent, bDate:this.info.bDate, bName:this.info.bName, bStatus:this.info.bStatus })
									.then(function(){alert("수정이 완료되었습니다."); location.reload();})
					}
				}
			})	
			console.log(id02);
			id02.style.display="block";		
         }
	}
});

Vue.component('custom-input', {
	props:['type', 'model'],
	template:'<input type={{type}} v-model={{content}}>'
})


var ex = new Vue( {
                    el: '#boardBody',
                    data: { 
                        datas: [],
						board:{},
                    },
                    created: function(){
                        axios.get('http://127.0.0.1:8888/board/boardList')
                        .then((result) => {
                        console.log(result)
                        this.datas = result.data.content
                        }
                    )},
					methods: {
   					 	greet: function (event) {
							alert("ok");
							console.log(event.target);
    					}
  					}

					
                });

</script>


</body>
</html>
