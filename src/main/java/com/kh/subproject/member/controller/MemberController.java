package com.kh.subproject.member.controller;

import com.kh.subproject.member.service.MemberService;
import com.kh.subproject.member.vo.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    @RequestMapping(value="/main")
    public ModelAndView goMain(){
        ModelAndView model = new ModelAndView();
        model.setViewName("member/loginMember");
        return model;
    }

    @PostMapping(value="/member/join")
    public ResponseEntity<String> joinMember(@RequestBody Member member){
        String joinFlag = memberService.joinMember(member);
        return (joinFlag.equals("success"))?new ResponseEntity<String>("success",HttpStatus.OK):new ResponseEntity<String>("fail",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(value="/member/login")
    public ResponseEntity<String> loginMember(@RequestBody Member member, HttpSession session){

        Member loginMember = memberService.loginMember(member);
        String loginFlag = "success";
        if(loginMember != null){
            loginMember.setPassword("");
            session.setAttribute("loginMember",loginMember);
        }else{
            loginFlag = "fail";
        }
        return (loginFlag.equals("success"))?new ResponseEntity<String>("success",HttpStatus.OK):new ResponseEntity<String>("fail",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value="/member/main")
    public ModelAndView goLoginMain(){
        ModelAndView model = new ModelAndView();
        model.setViewName("member/loginMain");
        return model;
    }

    @RequestMapping(value="/member/logout")
    public ModelAndView goLoginMain(HttpSession session, HttpServletResponse response) throws IOException {
        session.invalidate();
        ModelAndView model = new ModelAndView();
        model.setViewName("member/loginMember");
        return model;
    }

    @GetMapping(value="/member/select/{mno}")
    public ResponseEntity<Member> selectMember(@PathVariable("mno") Member member){
        return new ResponseEntity<Member>(memberService.selectMember(member),HttpStatus.OK);
    }

    @PutMapping(value="member/modify")
    public ResponseEntity<Member> modifyMember(@RequestBody Member member){
        System.out.println("Member :" + member);
        return (memberService.modifyMember(member) != null)?new ResponseEntity<Member>(memberService.modifyMember(member),HttpStatus.OK)
                :new ResponseEntity<Member>(new Member(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
