package com.kh.subproject.board.vo;

import com.kh.subproject.member.vo.Member;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Article {
    @Id
    @GeneratedValue
    private long ano;

    private String aTitle;

    private String aContent;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aDate;

    private String aStatus;

    @ManyToOne
    private Member mno;

    @ManyToOne
    private Board bno;
    
    @OneToMany(mappedBy = "article")
    private Set<Comments> comments;
}
